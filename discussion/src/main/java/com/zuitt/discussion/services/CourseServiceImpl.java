package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService{

    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    JwtToken jwtToken;

    // add a course
    public void createCourse(String stringToken,Course course){
        //Retrieve the "User" object using the extracted username from the JWT Token
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(author);
        courseRepository.save(newCourse);
    }

    //    Get All courses
    public Iterable<Course> getCourse(){
        return courseRepository.findAll();
    }

    public ResponseEntity updateCourse(Long id, String stringToken,Course course){
//        Find the course to update
        Course courseForUpdate = courseRepository.findById(id).get();
        String courseAuthor = courseForUpdate.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);
        if(authenticatedUserName.equals(courseAuthor)){
            courseForUpdate.setName(course.getName());
            courseForUpdate.setDescription(course.getDescription());
            courseForUpdate.setPrice(course.getPrice());
            courseRepository.save(courseForUpdate);

            return new ResponseEntity<>("Course updated successfully",HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorize to edit this course",HttpStatus.UNAUTHORIZED);
        }
    }

    //    Delete course
    public ResponseEntity deleteCourse(String stringToken,Long id){
        courseRepository.deleteById(id);
        return new ResponseEntity<>("Course deleted successfully.", HttpStatus.OK);
    }
    public  Iterable<Course> getMyCourse(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getCourses();
    }


}
